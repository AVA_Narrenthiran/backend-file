const sql = require('mssql');
exports.loadGridData = async (context, req) => {
    try {
        let reqData = req.body;
        // console.log(reqData);
        if (reqData.columnName == '' || reqData.columnOrder == undefined) {
            console.log("asdas");
            reqData.columnName = 'CodeGenID',
                reqData.columnOrder = 'Desc'
        }
        let query = `select * from CodeGen 
        inner join Frontend on CodeGen.FrontendID = Frontend.FrontendID
        inner join ServiceApi on CodeGen.ServiceID = ServiceApi.ServiceID
        inner join DatabaseApi on CodeGen.DatabaseID = DatabaseApi.DatabaseID where 1=1
and (((CodeGen.Project like '%${reqData.Project}%') and '${reqData.Project}' != '') or (1=1 and '${reqData.Project}' = ''))
and (((CodeGen.ComponentType ='${reqData.Component}') and '${reqData.Component}' != '') or (1=1 and '${reqData.Component}' = ''))
and (((Frontend.FrontendID = '${reqData.Framework}') and '${reqData.Framework}' != '') or (1=1 and '${reqData.Framework}' = ''))
and (((ServiceApi.ServiceID = '${reqData.Service}') and '${reqData.Service}' != '') or (1=1 and '${reqData.Service}' = ''))
and (((DatabaseApi.DatabaseID = '${reqData.Database}') and '${reqData.Database}' != '') or (1=1 and '${reqData.Database}' = ''))
and (((CodeGen.ProjectLeadMailID = '${reqData.ProjectLead}') and '${reqData.ProjectLead}' != '') or (1=1 and '${reqData.ProjectLead}' = ''))
and (Module like '%${reqData.SearchValue}%'
or ComponentName like '%${reqData.SearchValue}%'  )
order by ${reqData.columnName} ${reqData.columnOrder}
OFFSET (${reqData.PageNumber} - 1)* ${reqData.PageSize} ROWS 
FETCH NEXT ${reqData.PageSize} ROWS ONLY; 

        select count(CodeGenID) as TotalCount from CodeGen 
        inner join Frontend on CodeGen.FrontendID = Frontend.FrontendID
        inner join ServiceApi on CodeGen.ServiceID = ServiceApi.ServiceID
        inner join DatabaseApi on CodeGen.DatabaseID = DatabaseApi.DatabaseID where 1=1
and (((CodeGen.Project  like '%${reqData.Project}%') and '${reqData.Project}' != '') or (1=1 and '${reqData.Project}' = ''))
and (((CodeGen.ComponentType ='${reqData.Component}') and '${reqData.Component}' != '') or (1=1 and '${reqData.Component}' = ''))
and (((Frontend.FrontendID = '${reqData.Framework}') and '${reqData.Framework}' != '') or (1=1 and '${reqData.Framework}' = ''))
and (((ServiceApi.ServiceID = '${reqData.Service}') and '${reqData.Service}' != '') or (1=1 and '${reqData.Service}' = ''))
and (((DatabaseApi.DatabaseID = '${reqData.Database}') and '${reqData.Database}' != '') or (1=1 and '${reqData.Database}' = ''))
         and (Module like '%${reqData.SearchValue}%'
        or ComponentName like '%${reqData.SearchValue}%'					
)                   
 `
        let db = await sql.connect(
            {

                user: 'cdgennpadmin',
                password: "qJ]%='L.<6t6;YY2",
                database: 'ava-eus-cdgen-np-sqldb',
                server: 'ava-eus-cdgen-np-sqlsrv.database.windows.net',
                // or
                // server: 'SEZLP366\\SQLEXPRESS',
                options: {
                    encrypt: true, // for azure
                    trustServerCertificate: true // change to true for local dev / self-signed certs
                }
            }
        );
        let result = await db.request()
            .query(query);
        sql.close();
        // console.log(result.recordsets);
        let responseData = result.recordsets;
        context.res = {
            body: {
                Success: true,
                ResponseData: responseData,
                ErrorMessage: null
            },
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Content-Type': 'application/json',
                'Access-Control-Request-Headers': '*'

            }
        }
    }
    catch (err) {
        context.res = {
            body: {
                Success: false,
                ResponseData: err.message,
                ErrorMessage: null
            },
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Content-Type': 'application/json',
                'Access-Control-Request-Headers': '*'

            }
        }
    }
}