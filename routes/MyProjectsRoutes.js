const express = require('express');
const router = express.Router();
const grid = require('../controller/MyProjectsController');  
router.get('/getFilterData', grid.fetchDynamicData);
router.post('/', grid.loadGridData );
module.exports = router;