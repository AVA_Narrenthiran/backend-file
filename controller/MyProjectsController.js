const sql = require('mssql');
const {sqlConfig} = require('../util/database')

exports.loadGridData = async (req, res) => {
    try{
        let reqData = req.body;
        // console.log(reqData);
        if(reqData.columnName == '' || reqData.columnOrder == undefined)
        {
            console.log("asdas");
            reqData.columnName = 'CodeGenID',
            reqData.columnOrder = 'Desc'
        }
        let query = `select * from CodeGen 
					inner join ProjectLead on CodeGen.ProjectLeadID = ProjectLead.ProjectLeadID
					inner join Frontend on CodeGen.FrontendID = Frontend.FrontendID
					inner join ServiceApi on CodeGen.ServiceID = ServiceApi.ServiceID
					inner join DatabaseApi on CodeGen.DatabaseID = DatabaseApi.DatabaseID where 1=1
  and (((CodeGen.Project like '%${reqData.Project}%') and '${reqData.Project}' != '') or (1=1 and '${reqData.Project}' = ''))
and (((CodeGen.ComponentType ='${reqData.Component}') and '${reqData.Component}' != '') or (1=1 and '${reqData.Component}' = ''))
and (((ProjectLead.ProjectLeadID = '${reqData.ProjectLead}') and '${reqData.ProjectLead}' != '') or (1=1 and '${reqData.ProjectLead}' = ''))
and (((Frontend.FrontendID = '${reqData.Framework}') and '${reqData.Framework}' != '') or (1=1 and '${reqData.Framework}' = ''))
and (((ServiceApi.ServiceID = '${reqData.Service}') and '${reqData.Service}' != '') or (1=1 and '${reqData.Service}' = ''))
and (((DatabaseApi.DatabaseID = '${reqData.Database}') and '${reqData.Database}' != '') or (1=1 and '${reqData.Database}' = ''))
and (Module like '%${reqData.SearchValue}%'
or ComponentName like '%${reqData.SearchValue}%'
)
 order by ${reqData.columnName} ${reqData.columnOrder}
  OFFSET (${reqData.PageNumber} - 1)* ${reqData.PageSize} ROWS 

                        FETCH NEXT ${reqData.PageSize} ROWS ONLY; 
					select count(CodeGenID) as TotalCount from CodeGen 
					inner join ProjectLead on CodeGen.ProjectLeadID = ProjectLead.ProjectLeadID
					inner join Frontend on CodeGen.FrontendID = Frontend.FrontendID
					inner join ServiceApi on CodeGen.ServiceID = ServiceApi.ServiceID
					inner join DatabaseApi on CodeGen.DatabaseID = DatabaseApi.DatabaseID where 1=1
   and (((CodeGen.Project  like '%${reqData.Project}%') and '${reqData.Project}' != '') or (1=1 and '${reqData.Project}' = ''))
and (((CodeGen.ComponentType ='${reqData.Component}') and '${reqData.Component}' != '') or (1=1 and '${reqData.Component}' = ''))
and (((ProjectLead.ProjectLeadID = '${reqData.ProjectLead}') and '${reqData.ProjectLead}' != '') or (1=1 and '${reqData.ProjectLead}' = ''))
and (((Frontend.FrontendID = '${reqData.Framework}') and '${reqData.Framework}' != '') or (1=1 and '${reqData.Framework}' = ''))
and (((ServiceApi.ServiceID = '${reqData.Service}') and '${reqData.Service}' != '') or (1=1 and '${reqData.Service}' = ''))
and (((DatabaseApi.DatabaseID = '${reqData.Database}') and '${reqData.Database}' != '') or (1=1 and '${reqData.Database}' = ''))
 					and (Module like '%${reqData.SearchValue}%'
					or ComponentName like '%${reqData.SearchValue}%'
					
)
 `
        let db= await sql.connect(sqlConfig);
        let result = await db.request()
                             .query(query);
        sql.close();
        // console.log(result.recordsets);
        let responseData = result.recordsets;
        res.json({
            Success:true,
            ResponseData : responseData,
            ErrorMessage : null
        })
    }
    catch(err){
        console.log(err);
        res.json({
            Success:false,
            ResponseData : null,
            ErrorMessage : err.message
        })
    }
}


exports.fetchDynamicData = async (req, res) => {
    try{
        //console.log('KJDHVLISDGIO');
        let db= await sql.connect(sqlConfig);
        let result = await db.request()
                             .query(`
									select * from ProjectLead
									select * from Frontend
									select * from ServiceApi
									select * from DatabaseApi`);
        sql.close();
       //console.log(result); 
        let responseData = result.recordsets;
        //console.log(responseData);
        res.json({
            Success:true,
            ResponseData : responseData,
            ErrorMessage : null
        })
    }
    catch(err){
    console.log(err);
    
        res.json({
            Success:false,
            ResponseData : null,
            ErrorMessage : err.message
        })
    }
}


